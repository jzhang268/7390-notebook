if(!settings.multipleView) settings.batchView=false;
settings.tex="pdflatex";
defaultfilename="7390-notebook-test-1";
if(settings.render < 0) settings.render=4;
settings.outformat="";
settings.inlineimage=true;
settings.embed=true;
settings.toolbar=false;
viewportmargin=(2,2);

import settings;
import fontsize;
settings.tex="lualatex";
outformat="pdf";
texpreamble("\usepackage{jnth}");
texpreamble("\usepackage{fontspec}");
texpreamble("\newfontfamily\myfont{[Allerta-Regular.ttf]}");
texpreamble("\setlength{\parindent}{0pt}");
texpreamble("\usepackage{amsmath, amssymb}");
pen RGB(int r, int g, int b)
{
return rgb(r/255,g/255,b/255);
}
pen black = RGB(0, 0, 0);
pen darkgrey4 = RGB(67, 67, 67);
pen darkgrey3 = RGB(102, 102, 102);
pen darkgrey2 = RGB(153, 153, 153);
pen darkgrey1 = RGB(183, 183, 183);
pen grey = RGB(204, 204, 204);
pen lightgrey1 = RGB(217, 217, 217);
pen lightgrey2 = RGB(239, 239, 239);
pen black = RGB(0, 0, 0);
pen lightgrey3 = RGB(243, 243, 243);
pen white = RGB(255, 255, 255);
pen redberry = RGB(152, 0, 0);
pen lightredberry3 = RGB(230, 184, 175);
pen lightredberry2 = RGB(221, 126, 107);
pen lightredberry1 = RGB(204, 65, 37);
pen darkredberry1 = RGB(166, 28, 0);
pen darkredberry2 = RGB(133, 32, 12);
pen darkredberry3 = RGB(91, 15, 0);
pen red = RGB(255, 0, 0);
pen lightred3 = RGB(244, 204, 204);
pen lightred2 = RGB(234, 153, 153);
pen lightred1 = RGB(224, 102, 102);
pen darkred1 = RGB(204, 0, 0);
pen darkred2 = RGB(153, 0, 0);
pen darkred3 = RGB(102, 0, 0);
pen orange = RGB(255, 153, 0);
pen lightorange3 = RGB(252, 229, 205);
pen lightorange2 = RGB(249, 203, 156);
pen lightorange1 = RGB(246, 178, 107);
pen darkorange1 = RGB(230, 145, 56);
pen darkorange2 = RGB(180, 95, 6);
pen darkorange3 = RGB(120, 63, 4);
pen yellow = RGB(255, 255, 0);
pen lightyellow3 = RGB(255, 242, 204);
pen lightyellow2 = RGB(255, 229, 153);
pen lightyellow1 = RGB(255, 217, 102);
pen darkyellow1 = RGB(241, 194, 50);
pen darkyellow2 = RGB(191, 144, 0);
pen darkyellow3 = RGB(127, 96, 0);
pen green = RGB(0, 255, 0);
pen lightgreen3 = RGB(217, 234, 211);
pen lightgreen2 = RGB(182, 215, 168);
pen lightgreen1 = RGB(147, 196, 125);
pen darkgreen1 = RGB(106, 168, 79);
pen darkgreen2 = RGB(56, 118, 29);
pen darkgreen3 = RGB(39, 78, 19);
pen cyan = RGB(0, 255, 255);
pen lightcyan3 = RGB(208, 224, 227);
pen lightcyan2 = RGB(162, 196, 201);
pen lightcyan1 = RGB(118, 165, 175);
pen darkcyan1 = RGB(69, 129, 142);
pen darkcyan2 = RGB(19, 79, 92);
pen darkcyan3 = RGB(12, 52, 61);
pen cornflower = RGB(74, 134, 232);
pen lightcornflower3 = RGB(201,218,248);
pen lightcornflower2 = RGB(164,194,244);
pen lightcornflower1 = RGB(109,158,235);
pen darkcornflower1 = RGB(60,120,216);
pen darkcornflower2 = RGB(17,85,204);
pen darkcornflower3 = RGB(28,69,135);
pen blue = RGB(0, 0, 255);
pen lightblue3 = RGB(207, 226, 243);
pen lightblue2 = RGB(159, 197, 232);
pen lightblue1 = RGB(111, 168, 220);
pen darkblue1 = RGB(61, 133, 198);
pen darkblue2 = RGB(11, 83, 148);
pen darkblue3 = RGB(7, 55, 99);
pen purple = RGB(153, 0, 255);
pen lightpurple3 = RGB(217, 210, 233);
pen lightpurple2 = RGB(180, 167, 214);
pen lightpurple1 = RGB(142, 124, 195);
pen darkpurple1 = RGB(103, 78, 167);
pen darkpurple2 = RGB(53, 28, 117);
pen darkpurple3 = RGB(32, 18, 77);
pen magenta = RGB(255, 0, 255);
pen lightmagenta3 = RGB(234, 209, 220);
pen lightmagenta2 = RGB(213, 166, 189);
pen lightmagenta1 = RGB(194, 123, 160);
pen darkmagenta1 = RGB(166, 77, 121);
pen darkmagenta2 = RGB(116, 27, 71);
pen darkmagenta3 = RGB(76, 17, 48);
void mrduehoang(pair min3, pair max3, pen asdf) {
real h = max3.y-min3.y;
real l = 5/(10-sqrt(2));
pair m = (0,sqrt(2)/5*h*l);
pair p1 = (min3.x,max3.y)-(m.y,0);
pair p4 = min3-(m.y,0);
pair p2 = p1-l*(max3.y-min3.y,0);
pair p3 = p4-l*(max3.y-min3.y,0);
pair p5 = l*p3+(1-l)*p2;
pair p6 = l*p4+(1-l)*p1;
pair p7 = 2*p6-p1;
draw(p1--p2--p5--cycle,asdf);
for (int i=1; i<5; ++i) draw((0.2*(5-i)*p1+i*0.2*p5)--(0.2*(5-i)*p1+0.2*i*p7),asdf);
pair p8 = (max3.x,p7.y);
pair p9 = 0.2*p7+0.8*p5;
pair p10 = 2*p5-p2;
pair p11 = p5+2*(0,p9.y-p5.y);
pair p12 = p10+(p11.y-p10.y,0);
draw(p9--p7--p8,asdf);
draw(p11--p10--p12--cycle,asdf);
}

size(15.24cm); pen blah = lightcornflower1; defaultpen(fontsize(10pt)); defaultpen(0.2); path[] asdf1 = texpath("{\myfont \jnth {74} MEETING}"); path[] asdf2 = texpath("{\myfont JANUARY 05, 2019 {\vrule width 1pt} 1:00 PM - 5:00 PM}"); path[] asdf4 = texpath("{\myfont PURPOSE - organizational meeting, final intake adjustments, lift assembly}"); pair min1 = min(asdf1); pair max1 = max(asdf1); pair min2 = min(asdf2); pair max2 = max(asdf2); pair min4 = min(asdf4); pair max4 = max(asdf4); filldraw(asdf1,blah,white); real k = min1.x/min2.x; real kk = min1.x/min4.x; filldraw(shift(0,min1.y+2*k*min2.y)*scale(k)*asdf2,blah,white); filldraw(shift(0,min1.y+3*k*min2.y+2*kk*min2.y)*scale(kk)*asdf4,blah,white); pair min3 = shift(0,min1.y+3*k*min2.y+2*kk*min2.y)*scale(kk)*min4; pair max3 = max1; mrduehoang(min3, max3, blah+linewidth(2));
size(469.75502pt,0,keepAspect=true);
