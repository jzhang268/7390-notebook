# 7390 Notebook

Source files for the Engineering Notebook of FTC Team 7390 M-SET Jellyfish.

### Download
You can download the most recent pdf [*here*](https://jzhang268.gitlab.io/7390-notebook/7390-notebook.pdf), but this is not updated frequently.

### Compilation
This project can be compiled on a system with `latexmk`, `asymptote`, and Tex Live. Run `./deepboi 7390-notebook` or `./deepboi 7390-notebook-test`..
